extern crate whois_rust;

use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;

use actix_web::http::HeaderValue;
use actix_web::HttpRequest;
use itconfig::get_env_or_default;
use language_tags::LanguageTag;
use maxminddb::{MaxMindDBError, Reader};
use maxminddb::geoip2::model::{City, Country, Location, Postal};
use rand::prelude::*;
use tracing::{debug, warn};
use whois_rust::{WhoIs, WhoIsLookupOptions};

use crate::models::{CityInfo, ConnectionInfo, CoordinatesInfo, CountryInfo, IpInfoError, IpInfoObj, PostalInfo};

pub trait ExtractHeaders {
  fn extract_lang(&self) -> LanguageTag;
  fn extract_ip(&self) -> IpAddr;
}

impl ExtractHeaders for HttpRequest {
  fn extract_lang(&self) -> LanguageTag {
    let default_header_value: HeaderValue = HeaderValue::from_str("en-US").unwrap();

    let accept_language_header = self.headers()
      .get("accept-language")
      .unwrap_or(&default_header_value);
    let lang_header: &str = &accept_language_header.to_str().unwrap()[..5];
    debug!("extracted lang: {}", lang_header);
    LanguageTag::parse(lang_header).unwrap()
  }

  fn extract_ip(&self) -> IpAddr {
    let mut rng = thread_rng();
    let random_ip = [rng.gen::<u8>(), rng.gen::<u8>(), rng.gen::<u8>(), rng.gen::<u8>()];
    let default_ip_value: IpAddr = IpAddr::from(random_ip);
    let conn_info = self.connection_info().clone();
    let ip_value = conn_info.realip_remote_addr().unwrap();

    if IpAddr::from_str(ip_value).unwrap().is_ipv4() {
      let ipv4: Ipv4Addr = Ipv4Addr::from_str(ip_value).unwrap();
      if ipv4.is_loopback() {
        warn!("extracted ipv4 '{}' appears to be a loopback address; returning random value '{}'",
              self.peer_addr().unwrap().ip(),
              default_ip_value
        );
        return default_ip_value;
      }
      if ipv4.is_private() {
        warn!("extracted ipv4 '{}' appears to be a private address; returning random value '{}'",
              self.peer_addr().unwrap().ip(),
              default_ip_value
        );
        return default_ip_value;
      }
    } else if IpAddr::from_str(ip_value).unwrap().is_ipv6() {
      let ipv6: Ipv6Addr = Ipv6Addr::from_str(ip_value).unwrap();
      if ipv6.is_loopback() {
        warn!("extracted ipv6 '{}' appears to be a loopback address; returning random value '{}'",
              self.peer_addr().unwrap().ip(),
              default_ip_value
        );
        return default_ip_value;
      }
      if ipv6.is_unspecified() {
        warn!("extracted ipv6 '{}' appears to be an unspecified address; returning random value '{}'",
              self.peer_addr().unwrap().ip(),
              default_ip_value
        );
        return default_ip_value;
      }
    }
    let parsed_ip = self.peer_addr().unwrap().ip();
    debug!("extracted ip: {}", parsed_ip.to_string());
    parsed_ip
  }
}

pub fn _ip_lookup2(ip: IpAddr) -> Result<IpInfoObj, IpInfoError> {
  let servers_json: String = get_env_or_default(
    "SERVERS_JSON_PATH",
    "static/servers.json",
  );

  let whois = WhoIs::from_path(servers_json);
  if whois.is_err() {
    debug!("LookupError: {:?}", whois);
    return Err(IpInfoError::LookupError);
  }
  let ip_info = IpInfoObj {
    address: ip.to_string(),
    country: None,
    city: None,
    postal: None,
    coordinates: None,
    connection: None,
  };

  let result: String = whois.unwrap().lookup(
    WhoIsLookupOptions::from_string(ip.to_string()).unwrap()
  ).unwrap();
  debug!("result: {:?}", result);

  Ok(ip_info)
}

pub fn ip_lookup(ip: &IpAddr) -> Result<IpInfoObj, IpInfoError> {
  let city_db_path: String = get_env_or_default(
    "MAXMINDDB_CITY_PATH",
    "static/geoipdb/GeoLite2-City.mmdb",
  );
  let reader = Reader::open_readfile(city_db_path).unwrap();

  let mut ip_info = IpInfoObj {
    address: ip.to_string(),
    country: None,
    city: None,
    postal: None,
    coordinates: None,
    connection: None,
  };

  let geoip_city_lookup: Result<maxminddb::geoip2::City, MaxMindDBError> = reader.lookup(ip.clone());
  if geoip_city_lookup.is_err() {
    return Err(IpInfoError::LookupError);
  }
  let geoip_city = geoip_city_lookup.unwrap();
  debug!("looked up city value: {:?}", geoip_city);

  let geoip_isp_lookup: Result<maxminddb::geoip2::Isp, MaxMindDBError> = reader.lookup(ip.clone());
  if geoip_isp_lookup.is_err() {
    return Err(IpInfoError::LookupError);
  }
  let geoip_isp = geoip_isp_lookup.unwrap();
  debug!("looked up isp value: {:?}", geoip_isp);

  // ------------
  // country
  // --------
  let country: Option<Country> = geoip_city.clone().country;
  if country.is_some() {
    let c = country.unwrap();
    ip_info.country = Some(CountryInfo {
      name: String::from(c.clone().names.unwrap()["en"]),
      code: String::from(c.clone().iso_code.unwrap()),
    });
  }
  debug!("ip_lookup(with country): {:?}", ip_info);

  // ------------
  // postal
  // --------
  let postal: Option<Postal> = geoip_city.clone().postal;
  if postal.is_some() {
    let p = postal.unwrap();
    ip_info.postal = Some(PostalInfo {
      code: String::from(p.clone().code.unwrap()),
    });
  }
  debug!("ip_lookup(with postal): {:?}", ip_info);

  // ------------
  /* city */
  // --------
  let city: Option<City> = geoip_city.clone().city;
  if city.is_some() {
    let c = city.unwrap();
    ip_info.city = Some(CityInfo {
      name: String::from(c.clone().names.unwrap()["en"]),
    });
  }
  debug!("ip_lookup(with city): {:?}", ip_info);

  // ------------
  /* location */
  // --------
  let location: Option<Location> = geoip_city.clone().location;
  if location.is_some() {
    let l = location.unwrap();
    ip_info.coordinates = Some(CoordinatesInfo {
      latitude: l.clone().longitude.unwrap().to_string(),
      longitude: l.clone().latitude.unwrap().to_string(),
    });
  }
  debug!("ip_lookup(with location): {:?}", ip_info);

  // ------------
  // connection
  // --------
  let isp: Option<&str> = geoip_isp.clone().isp;
  if isp.is_some() {
    ip_info.connection = Some(ConnectionInfo {
      isp: String::from(isp.unwrap()),
    });
  }
  debug!("ip_lookup(with connection): {:?}", ip_info);

  Ok(ip_info)
}
