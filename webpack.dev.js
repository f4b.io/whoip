const path = require("path");
const {merge} = require("webpack-merge");
const common = require("./webpack.common.js");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const {CleanWebpackPlugin} = require("clean-webpack-plugin");

module.exports = merge(common, {
  mode: "development",
  watchOptions: {
    ignored: /node_modules/
  },
  devServer: {
    port: 31337,
    contentBase: path.resolve(__dirname, "web", "dist")
  },
  devtool: "inline-source-map",
  plugins: [
    new CleanWebpackPlugin({
      verbose: false,
    }),
    new HtmlWebpackPlugin({
      template: "./templates/base.html",
      filename: "base.html",
      xhtml: false,
      minify: false
    })
  ],
});
