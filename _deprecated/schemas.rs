table! {
  asn (network_start_integer, network_last_integer) {
    network_start_integer -> Numeric,
    network_last_integer -> Numeric,
    country_code -> Text,
    country_name -> Text,
    region_name -> Text,
    city_name -> Text,
    latitude -> Float,
    longitude -> Float,
    zip_code -> Text,
    time_zone -> Text,
  }
}
table! {
  city (network_start_integer, network_last_integer) {
    network_start_integer -> BigInt,
    network_last_integer -> BigInt,
    country_code -> Text,
    country_name -> Text,
    region_name -> Text,
    city_name -> Text,
    latitude -> Float,
    longitude -> Float,
    zip_code -> Text,
    time_zone -> Text,
  }
}

table! {
  country (network_start_integer, network_last_integer) {
    network_start_integer -> BigInt,
    network_last_integer -> BigInt,
    country_code -> Text,
    country_name -> Text,
    region_name -> Text,
    city_name -> Text,
    latitude -> Float,
    longitude -> Float,
    zip_code -> Text,
    time_zone -> Text,
  }
}
